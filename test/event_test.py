import unittest
from Structures import events


event1 = events.Event()
events2 = events.Event()


class TestEvents(unittest.TestCase):
    def test_event1_order(self):
        self.assertEqual(event1.get_event_id(), 0)

    def test_event2_order(self):
        self.assertEqual(events2.get_event_id(), 1)


if __name__ == '__main__':
    unittest.main()
