import yaml
import json
import unittest
from Modules import abstract_stats

# with open("../config/clan_member_stats.yaml", 'r') as stream:
#     try:
#         clan_member_stats_config = yaml.safe_load(stream)
#     except yaml.YAMLError as exc:
#         print(exc)
#
# with open("../settings/clan_list.json", 'r') as stream:
#     try:
#         clan_json = json.load(stream)
#     except FileNotFoundError as exc:
#         print(exc)


class ClanMemberTest(unittest.TestCase):
    def test_poor_skill(self):
        skill0 = abstract_stats.abstract_stats(0)
        skill19 = abstract_stats.abstract_stats(19)
        skill20 = abstract_stats.abstract_stats(20)
        skill21 = abstract_stats.abstract_stats(21)
        skill_negative = abstract_stats.abstract_stats(-10)

        self.assertEqual(skill0, "Poor", "Skill of 0 should be Poor")
        self.assertEqual(skill19, "Poor", "Skill of 19 should be Poor")
        self.assertEqual(skill20, "Poor", "Skill of 20 should be Poor")
        self.assertNotEqual(skill21, "Poor", "Skill of 21 should be Fair")
        self.assertEqual(skill_negative, "Poor", "Skill of negative value should be Poor")

    def test_fair_skill(self):
        skill20 = abstract_stats.abstract_stats(20)
        skill21 = abstract_stats.abstract_stats(21)

        skill40 = abstract_stats.abstract_stats(40)
        skill41 = abstract_stats.abstract_stats(41)
        self.assertNotEqual(skill20, "Fair", "Skill of 20 should be Poor")
        self.assertEqual(skill21, "Fair", "Skill of 21 should be Fair")
        self.assertEqual(skill40, "Fair", "Skill of 40 should be Fair")
        self.assertNotEqual(skill41, "Fair", "Skill of 41 should be Good")

    def test_good_skill(self):
        skill55 = abstract_stats.abstract_stats(55)
        skill40 = abstract_stats.abstract_stats(40)
        skill41 = abstract_stats.abstract_stats(41)
        skill54 = abstract_stats.abstract_stats(54)
        skill56 = abstract_stats.abstract_stats(56)
        self.assertEqual(skill54, "Good", "Skill of 54 should be Good")
        self.assertEqual(skill55, "Good", "Skill of 55 should be Good")
        self.assertEqual(skill40, "Fair", "Skill of 40 should be Fair")
        self.assertNotEqual(skill56, "Good", "Skill of 56 should be Very Good")
        self.assertEqual(skill41, "Good", "A Skill of 41 should be Good")

    def test_very_good_skill(self):
        skill55 = abstract_stats.abstract_stats(55)
        skill65 = abstract_stats.abstract_stats(65)
        skill56 = abstract_stats.abstract_stats(56)
        skill66 = abstract_stats.abstract_stats(66)

        self.assertEqual(skill55, "Good", "Skill of 55 should be Good")
        self.assertEqual(skill65, "Very Good", "Skill of 65 should be Very Good")
        self.assertEqual(skill56, "Very Good", "Skill of 56 should be Good")
        self.assertNotEqual(skill66, "Excellent", "A Skill of 66 should be Excellent")


if __name__ == '__main__':
    unittest.main()
