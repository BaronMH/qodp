import unittest
from Modules import god_list


constructor = god_list.GodList
god_full_list = constructor._get_god_full_list()
god_name_list = constructor.get_god_name_list()


class GodListTest(unittest.TestCase):
    @staticmethod
    def test_full_god_list_instance():
        assert isinstance(god_full_list, list)

    @staticmethod
    def test_god_name_list():
        assert isinstance(god_name_list, set)

    @unittest.expectedFailure
    def expect_failure(self):
        pass


if __name__ == '__main__':
    unittest.main()
