import yaml
import json
from collections import defaultdict
import random
from Modules import god_list as god_list_module


class ClanMember:
    def __init__(self):
        with open("../config/clan_member_stats.yaml", 'r') as stream:
            try:
                self.__clan_member_stats_config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)
        with open("../config/clan_member_names.yaml", 'r') as stream:
            try:
                self.__clan_member_name_config = yaml.safe_load(stream)
            except yaml.YAMLError as exc:
                print(exc)

    def _get_clan_member_stats_list(self):
        params = self.__clan_member_stats_config['clan_member_stats']
        stats = []
        for element in params:
            stats.append(element)
        return stats

    def _get_clan_member_name_list(self):
        params = self.__clan_member_name_config['names']
        names = []
        for element in params:
            names.append(element)
        return names

    def _generate_clan_member_dictionary(self):
        names = self._get_clan_member_name_list()
        member_dict = defaultdict(dict).fromkeys(names)
        for name in names:
            member_dict[name] = dict(self.__random_stat_generator())
            member_dict[name]["worships"] = self._random_god_generator()
        member_dict = dict(member_dict)
        return member_dict

    def __random_stat_generator(self):
        stats = self._get_clan_member_stats_list()
        stats_dict = defaultdict(dict).fromkeys(stats)
        for stat in stats_dict:
            rand = random.randint(0, 101)
            stats_dict[stat] = rand
        return stats_dict

    @staticmethod
    def _random_god_generator():
        god_list = god_list_module.get_god_name_list()
        god_list_len = len(god_list)
        rand = random.randint(0, god_list_len-1)
        return god_list[rand]

    def generate_clan_member_json_file(self):
        members = json.dumps(self._generate_clan_member_dictionary(), sort_keys=True, indent=4)
        file = open("../settings/clan_list.json", 'w')
        file.write(members)
        file.close()
