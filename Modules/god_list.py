import yaml


def get_god_name_list():
    with open("../config/gods.yaml", 'r') as stream:
        try:
            __god_list = yaml.safe_load(stream)
        except yaml.YAMLError as exc:
            print(exc)
        full_list = __god_list['gods']
        god_list = []
        for god in full_list:
            god_list.append(god)

        return god_list
