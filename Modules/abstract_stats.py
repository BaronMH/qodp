def abstract_stats(stats):
    if stats <= 20:
        stat = "Poor"
    elif 20 < stats <= 40:
        stat = 'Fair'
    elif 55 >= stats > 40:
        stat = 'Good'
    elif 65 >= stats > 55:
        stat = 'Very Good'
    elif 75 >= stats < 65:
        stat = 'Excellent'
    # elif 85 >= stats < 75:
    #     stat = 'Renowned'
    # elif 100 >= stats < 85:
    #     stat = 'Heroic'
    else:
        stat = None
    return stat
